
$("#boton1").click(function () {
    $("#div1").append("Poner algo... <br>");
});


var listaej2 = ["Segundo elemento", "Tercer elemento", "Cuarto elemento", "Quinto", "Sexto"];

$("#boton2").click(function () {
    $(listaej2).each(function (id, item) {
        $("#listaejercicio2").append("<li>" + item + "</li>");
    });
});

var listaTelefonos = [{
    nombre: "ana",
    tel: "android"
},
{
    nombre: "javi",
    tel: "iphone"
},
{
    nombre: "carmen",
    tel: "android"
},
{
    nombre: "rubén",
    tel: "android"
},
];



$("#boton3").click(function () {
    $(listaTelefonos).each(function (id, item) {
        $("table#agenda tbody").append("<tr><td>" + item.nombre + "</td><td>" + item.tel + "</td></tr>");
    });
});





var contadorand = 0;
var contadoriph = 0;
var contadorotros = 0;
var total = 0;

$("#boton4").click(function () {
    $.getJSON("clientes.json", function (data) {

        data.clientes.forEach(function (cliente) {

            total++;

            if (cliente.tel == "android") {
                contadorand++;
            }
            else if (cliente.tel == "iphone") {
                contadoriph++;
            }
            else {
                contadorotros++;
            }

            $("table#agenda2 tbody").append("<tr><td class=amarillo>" + cliente.id + "</td><td>" + cliente.nombre + "</td><td class=azul>" + cliente.tel + "</td></tr>");


        });

        var estadand = (contadorand / total * 100).toFixed(2) + "%";
        var estadiph = (contadoriph / total * 100).toFixed(2) + "%";
        var estadotros = (contadorotros / total * 100).toFixed(2) + "%";

        $("table#agenda2 tfoot").append("<tr><td></td><td>" + "Android" + "</td><td>" + estadand + "</td></tr><tr><td></td><td>" + "Iphone" + "</td><td>" + estadiph + "</td></tr><tr><td></td><td>" + "Otros" + "</td><td>" + estadotros + "</td></tr>");


    });
});


$("#boton5").click(function () {
    $.getJSON("clientes.json", function (data) {
        encontrado = false;

        data.clientes.forEach(function (cliente) {


            if ($(".form-control").val() == cliente.id) {

                $("#rellenarid").text(cliente.id);
                $("#rellenarnombre").text(cliente.nombre);
                $("#rellenartel").text(cliente.tel);
                $("#rellenaremail").text(cliente.email);
                encontrado = true;

            }


        });

        if (encontrado == false) {

            $(".form-control").css("background-color", "red")
        }


    });
});